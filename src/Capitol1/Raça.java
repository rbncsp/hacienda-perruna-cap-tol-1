package Capitol1;

public class Ra�a {
	private String nomRa�a;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Ra�a(String nom, GosMida gt, int t) {
		this(nom,gt);
		nomRa�a = nom;
		mida = gt;
		tempsVida = t;
	}
	public Ra�a(String nom, GosMida gt) {
		nomRa�a = nom;
		mida = gt;
		tempsVida = 10;
		dominant = false;
	}
	
	public String getNom() {
		return nomRa�a;
	}
	public GosMida getMida() {
		return mida;
	}
	public int getTemps() {
		return tempsVida;
	}
	public boolean getDominant() {
		return dominant;
	}
	
	public void setNom(String n) {
		nomRa�a = n;
	}
	public void setMida(GosMida m) {
		mida = m;
	}
	public void setTemps(int t) {
		tempsVida = t;
	}
	public void setDominant(boolean d) {
		dominant = d;
	}
	
	public String toString() {
		return "Nom: " + this.getNom() + ", Mida: " + this.getMida() + ", Temps de vida: " + this.getTemps() + ", Dominant: " + this.getDominant();
	}
}
