package Capitol1;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {

		Scanner sn = new Scanner(System.in);
		Gos gos1, gos2, gos3, gos4;
		Ra�a rasa1, rasa2;
		Granja granja1;
		int op = 1;

		while (op != 0) {

			System.out.println("\nEscull una opci�: ");
			System.out.println("[1] Cap�tol 1.A");
			System.out.println("[2] Cap�tol 1.B");
			System.out.println("[3] Cap�tol 1.C");
			System.out.println("[4] Cap�tol 1.D");
			System.out.println("[5] Cap�tol 1.E");
			System.out.println("[0] Sortir");

			try {
				op = sn.nextInt();
				switch (op) {
				case 1:
					gos1 = new Gos();
					gos2 = new Gos(20, "Agustin", 3, 'F');
					System.out.println("\nGos 1");
					gos1.visualitzar();
					gos1.borda();

					System.out.println("\nGos 2");
					gos2.visualitzar();
					gos2.borda();

					System.out.println("\nGossos creats: " + Gos.quantitatGossos());

					gos3 = new Gos(gos2);
					gos4 = new Gos("Pipo");

					System.out.println("\nGos 3");
					gos3.visualitzar();
					gos3.borda();

					System.out.println("\nGos 4");
					gos4.visualitzar();
					gos4.borda();

					System.out.println("\nGos 3 V2");
					gos3.clonar(gos4);
					gos3.visualitzar();
					gos3.borda();

					System.out.println("\nGossos creats: " + Gos.quantitatGossos());
					break;
				case 2:
					gos1 = new Gos();
					gos2 = new Gos(20, "Agustin", 3, 'F');
					gos3 = new Gos(gos2);

					rasa1 = new Ra�a("Chihuahua", GosMida.PETIT, 15);
					rasa2 = new Ra�a("Rottwiler", GosMida.MITJA, 8);

					gos2.setEdat(10);
					System.out.println("\nUn Rottweiler amb 10 anys:");
					gos2.visualitzar();
					System.out.println(gos2.getEstat());

					System.out.println("\nUn Chihuahua amb 4 anys:");
					gos1.setNom("Koki");
					gos1.visualitzar();
					System.out.println(gos1.getEstat());

					gos3.setEdat(9);
					System.out.println("\nSense ra�a amb 9 anys:");
					gos3.visualitzar();
					System.out.println(gos3.getEstat());

					System.out.println("\nSense ra�a amb 11 anys:");
					gos3.setEdat(11);
					gos3.visualitzar();
					System.out.println(gos3.getEstat());
					break;
				case 3:
					gos1 = new Gos();
					gos2 = new Gos(20, "Agustin", 3, 'F');
					
					granja1 = new Granja();
					
					rasa1 = new Ra�a("Chihuahua", GosMida.PETIT, 15);
					rasa2 = new Ra�a("Rottwiler", GosMida.MITJA, 8);
					
					gos2.setEdat(10);
					gos1.setNom("Koki");
					
					granja1.afegir(gos1);
					granja1.afegir(gos2);
					
					granja1.visualitzar();
					
					granja1.visualitzarVius();
					
					System.out.println("\n\n--------generarGranja()--------");
					Granja AutoGranja = Granja.generarGranja();
					
					AutoGranja.visualitzar();
					
					System.out.println("\nGos de la posici� 3: ");
					System.out.println(AutoGranja.obtenirGos(3));
					System.out.println("Gos de la posici� 5: ");
					System.out.println(AutoGranja.obtenirGos(5));
					break;
				case 4:
					generador();
					break;
				case 5:
					testCicleVida();
					break;
				case 0:
					System.out.println("Sortint...");
					break;
				default:
					System.out.println("Aquesta opci� no es troba al men�.");
				}
			} catch (Exception e) {
				System.out.println("Escriu un enter de la llista.");
				sn.nextLine();
			}
		}
	}
	
	static void generador() {
		Gos gos1, gos2, gos3, gos4, gos5, gos6, gos7, gos8, gos9, gos10;
		Ra�a rasa1, rasa2, rasa3;
		Granja granja1, granja2, granja3;
		
		rasa1 = new Ra�a("Chihuahua", GosMida.PETIT, 20);
		rasa2 = new Ra�a("Rottwiler", GosMida.MITJA, 8);
		rasa3 = new Ra�a("Terranova", GosMida.GRAN, 10);
		rasa2.setDominant(true);
		
		gos1 = new Gos(18, "Agustin", 1, 'M', rasa1);
		gos2 = new Gos(5, "Bony", 0, 'F', rasa3);
		gos3 = new Gos(9, "Golum", 2, 'M', rasa2);
		gos4 = new Gos(4, "Bagatela", 0, 'F', rasa3);
		gos5 = new Gos(8, "Pillo", 1, 'M', rasa1);
		gos6 = new Gos(12, "Nala", 3, 'F', rasa1);
		gos7 = new Gos(5, "Kiro", 1, 'M', rasa2);
		gos8 = new Gos(10, "Bender", 3, 'M', rasa1);
		gos9 = new Gos(3, "Bimbo", 1, 'M');
		gos10 = new Gos(6, "Benita", 2, 'F');
		
		granja1 = new Granja(5);
		granja2 = new Granja(5);
		granja3 = new Granja(5);
		
		granja1.afegir(gos1);
		granja1.afegir(gos2);
		granja1.afegir(gos3);
		granja1.afegir(gos4);
		granja1.afegir(gos5);
		
		granja2.afegir(gos6);
		granja2.afegir(gos7);
		granja2.afegir(gos8);
		granja2.afegir(gos9);
		granja2.afegir(gos10);
		
		for(int i = 0; i < 5; i++) {
			if(granja1.getGossos()[i].aparellar(granja2.getGossos()[i]) != null) {
				granja3.afegir(granja1.getGossos()[i].aparellar(granja2.getGossos()[i]));
			}
		}
		
		granja1.visualitzar();
		granja2.visualitzar();
		granja3.visualitzar();
	}
	
	static void testCicleVida() {
		Scanner sn = new Scanner(System.in);
		int cicles = 0;
		while(cicles < 1 || cicles > 20) {
			System.out.print("Indica els anys que vols avan�ar (1~20): ");
			cicles = sn.nextInt();
		}
		
		Gos gos1, gos2, gos3, gos4, gos5, gos6, gos7, gos8, gos9, gos10;
		Ra�a rasa1, rasa2, rasa3;
		Granja granja1;
		
		rasa1 = new Ra�a("Chihuahua", GosMida.PETIT, 20);
		rasa2 = new Ra�a("Rottwiler", GosMida.MITJA, 8);
		rasa3 = new Ra�a("Terranova", GosMida.GRAN, 10);
		rasa2.setDominant(true);
		
		gos1 = new Gos(18, "Agustin", 1, 'M', rasa1);
		gos2 = new Gos(5, "Bony", 0, 'F', rasa3);
		gos3 = new Gos(9, "Golum", 2, 'M', rasa2);
		gos4 = new Gos(4, "Bagatela", 0, 'F', rasa3);
		gos5 = new Gos(8, "Pillo", 1, 'M', rasa1);
		gos6 = new Gos(12, "Nala", 3, 'F', rasa1);
		gos7 = new Gos(5, "Kiro", 1, 'M', rasa2);
		gos8 = new Gos(10, "Bender", 3, 'M', rasa1);
		gos9 = new Gos(3, "Bimbo", 1, 'M');
		gos10 = new Gos(6, "Benita", 2, 'F');
		
		granja1 = new Granja(40);
		
		granja1.afegir(gos1);
		granja1.afegir(gos2);
		granja1.afegir(gos3);
		granja1.afegir(gos4);
		granja1.afegir(gos5);
		granja1.afegir(gos6);
		granja1.afegir(gos7);
		granja1.afegir(gos8);
		granja1.afegir(gos9);
		granja1.afegir(gos10);
		
		for(int v = 0; v < cicles; v++) {
			int perrillos = granja1.getNumGossos();
			for(int i = 0; i < perrillos-1;i++) {
				if(granja1.getGossos()[i].aparellar(granja1.getGossos()[i+1]) != null) {
					granja1.afegir(granja1.getGossos()[i].aparellar(granja1.getGossos()[i+1]));
					granja1.getGossos()[granja1.getNumGossos()-1].visualitzar();
					System.out.println();
					granja1.getGossos()[i].setEdat(granja1.getGossos()[i].getEdat()+1);
					granja1.getGossos()[i+1].setEdat(granja1.getGossos()[i+1].getEdat()+1);
					i += 1;
				}else {
					Gos aux = new Gos(granja1.getGossos()[i]);
					aux.setRasa(granja1.getGossos()[i].getRasa());
					
					granja1.getGossos()[i] = new Gos(granja1.getGossos()[i+1]);
					granja1.getGossos()[i].setRasa(granja1.getGossos()[i+1].getRasa());
					
					granja1.getGossos()[i+1] = aux;
					granja1.getGossos()[i+1].setRasa(aux.getRasa());
					
					granja1.getGossos()[i].setEdat(granja1.getGossos()[i].getEdat()+1);
				}
			}
			granja1.visualitzar();
		}
	}
}
