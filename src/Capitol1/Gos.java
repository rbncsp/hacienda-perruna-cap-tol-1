package Capitol1;

public class Gos {
	
	private int edat,fills;
	private String nom;
	private char sexe;
	private static int comptador = 0;
	private Ra�a rasa;
	private GosEstat estat = GosEstat.VIU;
	
	{comptador++;}
	
	public Gos() {
		edat = 4;
		nom = "SenseNom";
		fills = 0;
		sexe = 'M';
	}
	public Gos(int e, String n, int f, char s) {
		edat = e;
		nom = n;
		fills = f;
		sexe = s;
	}
	public Gos(Gos original) {
		edat = original.edat;
		nom = original.nom;
		fills = original.fills;
		sexe = original.sexe;
	}
	public Gos(String n) {
		edat = 4;
		nom = n;
		fills = 0;
		sexe = 'M';
	}
	public Gos(int e, String n, int f, char s,Ra�a r) {
		this(e, n, f, s);
		rasa = r;
	}
	
	public int getEdat() {
		return edat;
	}	
	public String getNom() {
		return nom;
	}
	public int getFills() {
		return fills;
	}
	public char getSexe() {
		return sexe;
	}
	public Ra�a getRasa() {
		return rasa;
	}
	
	public GosEstat getEstat() {
		return estat;
	}
	
	public void setEdat(int e) {
		edat = e;
		if(rasa == null) {
			if(e >= 10) {
				estat = GosEstat.MORT;
			}
		}else {
			if(e > rasa.getTemps()) {
				estat = GosEstat.MORT;
			}
		}
	}
	public void setNom(String n) {
		nom = n;
	}
	public void setFills(int f) {
		fills = f;
	}
	public void setSexe(char s) {
		sexe = s;
	}
	public void setRasa(Ra�a r) {
		rasa = r;
	}
	public void setEstat(GosEstat e) {
		estat = e;
	}
	
	public void borda() {
		System.out.println("guau guau");
	}
	
	public String toString() {
		if(rasa == null) {
			return "Nom: " + this.getNom() + ", Sexe: " + this.getSexe() + ", Edat: " + this.getEdat() + ", Fills: " + this.getFills();
		}else {
			return "Nom: " + this.getNom() + ", Sexe: " + this.getSexe() + ", Edat: " + this.getEdat() + ", Fills: " + this.getFills() + "\nRa�a - " + this.getRasa();
		}
	}
	
	public void visualitzar() {
		System.out.println(this.toString());
	}
	
	public void clonar(Gos original) {
		edat = original.edat;
		nom = original.nom;
		fills = original.fills;
		sexe = original.sexe;
	}
	
	public static int quantitatGossos() {
		return comptador;
	}
	
	public Gos aparellar(Gos g) {
		Ra�a r = null;
		String np = "",nm = "";
		boolean fill = false;
		if(this.edat > 1 && this.edat < 11 && g.edat > 1 && g.edat < 11) {
			if(this.sexe != g.sexe) {
				if(this.getEstat() == GosEstat.VIU && g.getEstat() == GosEstat.VIU) {
					if(this.sexe == 'F') {
						np = g.getNom();
						nm = this.getNom();
						if(this.fills <= 3) {
							if(this.getRasa() != null && g.getRasa() != null) {
								if(this.getRasa().getMida() == g.getRasa().getMida() || g.getRasa().getMida() == GosMida.PETIT || g.getRasa().getMida() == GosMida.MITJA && this.getRasa().getMida() == GosMida.GRAN) {
									fill = true;
									if(this.getRasa().getDominant() == g.getRasa().getDominant() || this.getRasa().getDominant()) {
										r = this.getRasa();
									}else {
										r = g.getRasa();
									}
								}
							}else if(this.getRasa() != null && g.getRasa() == null) {
								fill = true;
								r = this.getRasa();
							}else if(this.getRasa() == null && g.getRasa() != null){
								fill = true;
								r = g.getRasa();
							}else {
								fill = true;
							}
						}
					}else {
						np = this.getNom();
						nm = g.getNom();
						if(g.fills <= 3) {
							if(this.getRasa() != null && g.getRasa() != null) {
								if(this.getRasa().getMida() == g.getRasa().getMida() || this.getRasa().getMida() == GosMida.PETIT || this.getRasa().getMida() == GosMida.MITJA && g.getRasa().getMida() == GosMida.GRAN) {
									fill = true;
									if(this.getRasa().getDominant() == g.getRasa().getDominant() || g.getRasa().getDominant()) {
										r = g.getRasa();
									}else {
										r = this.getRasa();
									}
								}
							}else if(this.getRasa() != null && g.getRasa() == null) {
								fill = true;
								r = this.getRasa();
							}else if(this.getRasa() == null && g.getRasa() != null){
								fill = true;
								r = g.getRasa();
							}else {
								fill = true;
							}
						}
					}
				}
			}
		}
		if(fill) {
			this.fills++;
			g.fills++;
			int sexeFill = (int) Math.round(Math.random());
			if(sexeFill == 1) {
				return new Gos(0,"Filla de " + nm,0,'F',r);
			}else {
				return new Gos(0,"Fill de " + np,0,'M',r);
			}
		}else {
			return null;
		}
	}
}
