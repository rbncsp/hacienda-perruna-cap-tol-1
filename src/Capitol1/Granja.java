package Capitol1;

public class Granja {

	private Gos[] gossos;
	private int numGossos,topGossos;
	
	
	public Granja() {
		gossos = new Gos[100];
		numGossos = 0;
		topGossos = 100;
	}
	
	public Granja(int d) {
		if(d > 0 && d < 101) {
			gossos = new Gos[d];
			numGossos = 0;
			topGossos = d;
		}else {
			gossos = new Gos[100];
			numGossos = 0;
			topGossos = 100;
		}
	}
	
	
	public Gos[] getGossos() {
		return gossos;
	}
	
	public int getNumGossos() {
		return numGossos;
	}
	
	public int getTopGossos() {
		return topGossos;
	}
	
	public int afegir(Gos g) {
		if(numGossos < topGossos) {
			gossos[numGossos] = g;
			numGossos++;
			return numGossos;
		}else {
			return -1;
		}
	}
	
	public String toString() {
		String r = "\nLlista de gossos:\n";
		for(int i = 0; i < numGossos;i++) {
			r += "\n" + gossos[i].toString();
		}
		r += "\n\nN�mero de gossos: " + numGossos + "\nCapacitat: " + topGossos;
		return r;
	}
	
	public void visualitzar() {
		System.out.println(this.toString());
	}
	
	public void visualitzarVius() {
		System.out.println("\n\nLlista de gossos vius:");
		for(int i = 0; i < numGossos;i++) {
			if(gossos[i].getEstat() == GosEstat.VIU) {
				System.out.println("\n" + gossos[i].toString());
			}
		}
	}
	
	public static Granja generarGranja() {
		Granja gr = new Granja();
		Gos gos1 = new Gos(5, "Chipi", 0, 'F');
		Gos gos2 = new Gos(2, "Duna", 0, 'F');
		Gos gos3 = new Gos(8, "Pluto", 1, 'M');
		Gos gos4 = new Gos(12, "Chuli", 2, 'M');
		Gos gos5 = new Gos(4, "Canela", 3, 'F');
		gr.afegir(gos1);
		gr.afegir(gos2);
		gr.afegir(gos3);
		gr.afegir(gos4);
		gr.afegir(gos5);
		return gr;
	}
	
	public Gos obtenirGos(int n) {
		if(n >= numGossos) {
			return null;
		}else {
			return gossos[n];
		}
	}
}
